const { getAllTeachers, createTeacher, updateTeacher, getTeacherById, deleteTeacher } = require('../routes-controllers/profesor')

const { Router } = require('express')
const router = Router()

router.get('', getAllTeachers)          // Obtiene todos los profesores
router.get('/:id', getTeacherById)      // Obtiene profesor por id
router.post('', createTeacher)          // Crear profesor
router.put('/:id', updateTeacher)       // Modifica profesor
router.delete('/:id', deleteTeacher)    // Borrar profesor

module.exports = router