const { Router } = require('express')
const router = Router()
// const { verificaToken } = require('../middlewares/autenticationH') //Verificacion token en Header
const { verificaToken } = require('../middlewares/autentication')
const { getAllUsers, getUserById, createUser, updateUser, deleteUser } = require('../routes-controllers/usuario')

router.get('', verificaToken, getAllUsers)        // Obtener todos los usuarios
router.get('/:id', verificaToken, getUserById)    // Obtener un usuario
router.post('', verificaToken, createUser)        // Crear usuario
router.put('/:id', verificaToken, updateUser)     // Actualizar un usuario
router.delete('/:id', verificaToken, deleteUser)  // Borrar un usuario

module.exports = router