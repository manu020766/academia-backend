require('dotenv').config()
const Usuario = require('../models/usuario')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { Router } = require('express')
const router = Router()

//===================================================
// Login: envia email y password y obtiene un token
//===================================================
router.post('', async (req, res) => {
    const { email, password } = req.body

    try {
        const usuarioDB = await Usuario.findOne({ email })

        //-- Compara el password enviado con el de la base de datos
        await bcrypt.compare(password, usuarioDB.password)

        //-- Genera el token (el usuario es el payload)
        let usuario = { nombre: usuarioDB.nombre, email: usuarioDB.email, role: usuarioDB.role, img: usuarioDB.img }
        token = jwt.sign({ usuario }, process.env.SECRET, { expiresIn: 14400 })
        
        let id = usuarioDB._id
        res.status(200).json({ ok: true, usuario, id, token })

    } catch (error) {
        res.status(404).json({ok: false, mensaje: 'Credenciales incorrectas', errors: error})
    }    
})

module.exports = router