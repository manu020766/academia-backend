const { getAllCourses, getCourseById, createCourse, updateCourse, deleteCourse } = require('../routes-controllers/curso')

const { Router } = require('express')
const router = Router()

router.get('', getAllCourses)       // Obtener todos los cursos
router.get('/:id', getCourseById)   // Obtener un curso por su identificador
router.post('', createCourse)       // Crear un curso
router.put('/:id', updateCourse)    // Actualizar un curso
router.delete('/:id', deleteCourse) // Borrar un curso

module.exports = router