const { getAllPupils, getPupilById, createPupil, updatePupil, deletePupil } = require('../routes-controllers/alumno')
const { Router } = require('express')
const router = Router()

router.get('', getAllPupils)        // Obtener Todos los alumnos. Parámetro opcional activo: true | false
router.get('/:id', getPupilById)    // Obtener alumno por id
router.post('', createPupil)        // Crear alumno
router.put('/:id', updatePupil)     // Actualizar un alumno
router.delete('/:id', deletePupil)  // Borrar un alumno



module.exports = router