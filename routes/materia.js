const { getAllSubjects, getSubjectById, createSubject, updateSubject, deleteSubject } = require('../routes-controllers/materia')
const { Router } = require('express')
const router = Router()

router.get('', getAllSubjects)          // Obtener Todas las materias
router.get('/:id', getSubjectById)      // Obtener una materia
router.post('', createSubject)          // Crear materia
router.put('/:id', updateSubject)       // Actualizar una materia
router.delete('/:id', deleteSubject)    // Borrar una materia

module.exports = router