require('dotenv').config()
const jwt = require('jsonwebtoken')

//=============================================================
// verifica el token, para proteger las rutas que lo requieran
// En éste caso llega el token como parámetro
//=============================================================
exports.verificaToken = function(req, res, next) {
    const token = req.query.token

    jwt.verify(token, process.env.SECRET, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                mensaje: 'No tiene autorización',
                errors: err
            })
        }

         // En: decoded tengo la información del usuario que ha realizado la llamada
         // lo estoy añadiendo al req, con lo cual puedo identificar a la persona que ha relizado la llamada.
        req.usuario = decoded.usuario
        
        next()
    })
}