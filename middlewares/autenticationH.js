require('dotenv').config()
const jwt = require('jsonwebtoken')

//=============================================================
// verifica el token, para proteger las rutas que lo requieran
// En éste caso llega el token en el header
//=============================================================
exports.verificaToken = function(req, res, next) {
    const authorization = req.headers['authorization']

    if(!authorization){
        res.status(401).json({ ok: false, error: "Es necesario el token de autenticación" })
        return
    }

    const token = authorization.replace('Bearer ', '')

    jwt.verify(token, process.env.SECRET, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                mensaje: 'No tiene autorización',
                errors: err
            })
        }

         // En: decoded tengo la información del usuario que ha realizado la llamada
         // lo estoy añadiendo al req, con lo cual puedo identificar a la persona que ha relizado la llamada.
        req.usuario = decoded.usuario
        
         next()
    })
}