// Require
require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
var bodyParser = require('body-parser')

//Settings
const app = express()

//Midleware
app.use(bodyParser.json())                          //parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))  // parse application/json

//Connect database
// mongoose.set('runValidators', true)
// const academiaConexionOptions = { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }
// mongoose.connection.openUri(process.env.ACADEMIA_CONEXION, academiaConexionOptions, (err, res) => {
//     if (err) {
//         console.log('BASE DE DATOS NO CONECTADA')
//         throw err
//     }
//     console.log('Base de datos: \x1b[33m%s\x1b[0m', 'online')
// })
mongoose.connect(process.env.ACADEMIA_CONEXION, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })


//Routes
const appRoutes = require('./routes/app')
const usuarioRoutes = require('./routes/usuario')
const loginRoutes = require('./routes/login')
const materiaRoutes = require('./routes/materia')
const alumnoRoutes = require('./routes/alumno')
const cursoRoutes = require('./routes/curso')
const profesorRoutes = require('./routes/profesor')

app.use('/profesor', profesorRoutes)
app.use('/curso', cursoRoutes)
app.use('/alumno', alumnoRoutes)
app.use('/materia', materiaRoutes)
app.use('/login', loginRoutes)
app.use('/usuario', usuarioRoutes)
app.use('/', appRoutes)

//TODO queda pendiente controlar el error de que no se suministre algun parametro necesario para una ruta, por ejemplo un :id para un delete
// mirar: https://expressjs.com/es/guide/error-handling.html

//Server
app.listen(3000, console.log('Server listening in port 3000: \x1b[33m%s\x1b[0m','online'))