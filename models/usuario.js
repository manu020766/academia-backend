const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const Schema = mongoose.Schema

const rolesValidos = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} no es un rol permitido'
}

const usuarioSchema = new Schema({
    nombre: { type: String, require: [true, 'El nombre es requerido'] },
    email: { type: String, unique: true, require: [true, 'El email es requerido'] },
    password: { type: String, require: [true, 'El password es requerido'] },
    img: { type: String, require: false },
    role: { type: String, require: false, default: 'USER_ROLE', enum: rolesValidos }
})

usuarioSchema.plugin(uniqueValidator, { message: 'Error, el {PATH} debe ser único' })

module.exports = mongoose.model('Usuario', usuarioSchema)
