const mongoose = require('mongoose')
var uniqueValidator = require('mongoose-unique-validator')

const materiaSchema = new mongoose.Schema({
    categoria: { type: String, require: [true, 'El nombre de la materia es requerida'] },
    subCategoria: { type: String, require: [true, 'El nombre de la materia es requerida'] },
    nombre: { type: String, unique: true, require: [true, 'El nombre de la materia es requerida'] }
})

materiaSchema.plugin(uniqueValidator, { message: 'Error, el {PATH} de la materia debe ser único' })

module.exports = mongoose.model('Materia', materiaSchema)



//     [
// {categoria : "Desarrollo": []}


//         "Desarrollo": [
//             "Desarrollo Web": [
//                 { nombre: "JavaScript"},
//                 { nombre: "Angular"},
//                 { nombre: "React"}
//             ],
//             "Aplicaciones móviles": [
//                 { nombre: "Ionic"},
//             ],
//             "Bases de datos": [
//                 { nombre: "MySql"}
//             ]
//         ],
//         "Informatica y Software": [
//             "Hardware": [
//                 { nombre: "Arduino"},
//                 { nombre: "RaspBerri Pi"}
//             ],
//             "Sistemas operativos": [
//                 { nombre: "Linux"}
//             ]
//         ],
//         "Diseño": [
//             "Diseño Web": [
//                 { nombre: "CSS"}
//             ],
//             "Diseño Gráfico": [
//                 { nombre: "Illustrator"},
//                 { nombre: "Photoshop"}
//             ]
//         ]
//     ]


