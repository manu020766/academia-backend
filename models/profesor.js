const mongoose = require('mongoose')

const Schema = mongoose.Schema

const generoTipo = {
    values : ['Hombre', 'Mujer'],
    message : '{VALUE} no es un tipo de género permitido'
}

const conocimientosNivel = {
    values : ['Basico', 'Intermedio', 'Avanzado'],
    message: '{VALUE} no está permitido como nivel de conocimiento'
}

const RedesSocialesSchema = new Schema({
    id: { type: String, require: false },
    nombre: { type: String, require: false }
})

const conocimientosSchema = new Schema({
    tema: { type: String, require: [true, 'El tema es requerido'] },
    nivel: { type: String, require: [true, 'El campo de conocimientos es requerido'], enum: conocimientosNivel }
})

const tituloSchema = new Schema({
    centro: { type: String, require: [true, 'El nombre  del centro de estudios es requerido'] },
    titulacion: { type: String, require: [true, 'El nombre  del título es requerido'] },
    anio: { type: Number, require: [true, 'El año de obtención del título'] }
})

const experienciaSchema = new Schema({
    empresa: { type: String, require: [true, 'El nombre  de la empresa es requerido'] },
    funcion: { type: String, require: [true, 'La función principal es requerida'] },
    anios: { type: Number, require: [true, 'Años trabajando en la empresa'] }
})

const profesorSchema = new Schema({
    nombre: { type: String, require: [true, 'El nombre es requerido'] },
    apellidos: { type: String, require: [true, 'Los apellidos son requeridos'] },
    dni: { type: String, require: [true, 'El DNI es requerido'] },
    foto: { type: String, require: [true, 'La foto es requerida'] },
    genero: { type: String, enum: generoTipo, require: [true, 'El género es requerido'] },
    edad: { type: Number, require: [true, 'La edad es requerida'] },
    direccion: { type: String, require: [true, 'La dirección es requerida'] },
    codigo_postal: { type: String, require: [true, 'El código postal es requerido'] },
    poblacion: { type: String, require: [true, 'La poblacion es requerida'] },
    pais: { type: String, require: [true, 'El pais es requerido'] },
    email: {
        type: String,
        required: [true, 'El email es requerido'],
        validate: {
          validator: function(v) {
            return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v)
          },
          message: props => `${props.value} no tiene un formato correcto de email`
        }
    },
    telefono: { type: String, require: [true, 'El teléfono es requerido'] },
    idioma: { type: String, require: true, default: "es" },
    disponible: { type: Boolean, require: true, default: true },
    redes_sociales: [RedesSocialesSchema],
    conocimientos: [conocimientosSchema],
    titulo: [tituloSchema],
    experiencia: [experienciaSchema]
}, { collection: 'profesores' })

module.exports = mongoose.model('Profesor', profesorSchema)