const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const Schema = mongoose.Schema

const alumnoSchema = new Schema({
    nombre: { type:String, require: [true, 'El nombre es requerido']},
    apellidos: { type:String, require: [true, 'Los apellidos es un dato requerido']},
    dni: { type:String, require: [true, 'El dni un dato requerido']},
    email: { type:String, unique: true,require: [true, 'El email es requerido']},
    telefono: { type: String, require: [true, 'El teléfono es requerido']},
    cp: { type: String, require: [true, 'El código postal es requerido'] },
    poblacion: { type: String, require: [true, 'La población es requerida'] },
    foto: { type: String, require: false },
    activo: { type: Boolean, require: true, default: true}
})

alumnoSchema.plugin(uniqueValidator, { message: 'Error, el {PATH} debe ser único' })

module.exports = mongoose.model('Alumno', alumnoSchema)