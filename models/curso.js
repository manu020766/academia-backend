const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const diasValidos = {
    values: ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
    message: '{VALUE} no es un día de la semana válido'
}

const tiposCursos = {
    values: ['Inem', 'Forem', 'Propio', 'Externo', 'Empresa'],
    message: '{VALUE} no es un tipo de curso válido'
}

const tiposPlataformas = {
    values: ['Online', 'Presencial', 'Semi-presencial', 'Vídeo'],
    message: '{VALUE} no es un tipo de plataforma válido'
}

const nivelCurso = {
    values: ['Básico', 'Intermedio', 'Avanzado'],
    message: '{VALUE} no es un nivel válido'
}

const horario = {
    dia: { type: String, enum: diasValidos },
    hora_entrada: { type: String },
    hora_salida: { type: String },
}

const gastos = {
    nombre: { type: String, require: [true, 'El nombre del gasto es requerido'] },
    importe: { type: Number, require: false }
}

const horasImpartidasSchema = new mongoose.Schema({
    fecha: { type: Date, require: false },
    horas: { type: Number, require: false },
    profesor: { type: mongoose.Schema.ObjectId, ref: "Profesor" }
})

const cursoSchema = new mongoose.Schema({
    nombre: { type: String, unique:true, require: [true, 'El nombre del curso es requerido'] },
    tipo_curso: { type: String, enum: tiposCursos, require: [true, 'El tipo de curso es requerido'] },
    plataforma_curso: { type: String, enum: tiposPlataformas }, 
    nivel: { type: String, enum: nivelCurso },
    ingreso_por_alumno: { type: Number, require: true, default: 0 },
    numero_minimo_alumnos: { type: Number, require: true, default: 0 },
    fecha_comienzo: { type: Date, require: false },
    duracion_horas: { type: Number, require: [true, 'El numero de horas es requerido'] },
    duracion_dias: { type: Number, require: false },
    duracion_semanas: { type: Number, require: false },
    curso_terminado: { type: Boolean, require: true, default: false },
    gastos: [gastos],
    horario: [horario],
    horas_impartidas: [horasImpartidasSchema],
    profesores: [{ type: mongoose.Schema.ObjectId, ref: "Profesor" }],
    alumnos: [{ type: mongoose.Schema.ObjectId, ref: "Alumno" }]
})

cursoSchema.plugin(uniqueValidator, { message: 'El {PATH} debe de ser único' })

module.exports = mongoose.model('Curso', cursoSchema)
