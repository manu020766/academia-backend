const Usuario = require('../models/usuario')
const bcrypt = require('bcryptjs')

//===================================================
// Obtener todos los usuarios
//===================================================
exports.getAllUsers = async (req, res)=> {
    try {
        const usuarios = await Usuario.find({}, 'nombre email img role')
        res.status(200).json({ ok: true, usuarios})
    } catch (error) {
        res.status(500).json({ ok: false, mensaje: 'Error recuperando usuarios', error })
    }
}

//===================================================
// Obtener un usuario
//===================================================
exports.getUserById = async (req, res)=> {
    const idUsuario = req.params.id
    try {
        const usuario = await Usuario.findById(idUsuario, 'nombre email img role')
        res.status(200).json({ ok: true, usuario })
    } catch (error) {
        res.status(400).json({ ok: false, mensaje: 'Error recuperando usuario', error })
    }       
}

//===================================================
// Crear usuario
//===================================================
exports.createUser = async (req, res)=> {
    let usuario = new Usuario({ ...req.body, password: bcrypt.hashSync(req.body.password, 10 ) })

    try {
        let usuarioDb = await usuario.save()
        usuario.password = ':)'
        res.status(201).json({ ok: true, usuarioDb })
    } catch (error) {
        res.status(400).json({ok: false, mensaje: 'Error guardando usuario', errors: error})
    }
}

//===================================================
// Actualizar un usuario
//===================================================
exports.updateUser = async (req, res)=> {
    const idUsuario = req.params.id
    let usuario = { ...req.body }

    try {
        let usuarioDB = await Usuario.findByIdAndUpdate(idUsuario, usuario)
        usuarioDB.password = ':)'

        res.status(200).json({ ok:true, mensaje: 'Usuario actualizado', usuario, usuarioDB })
    } catch (error) {
        res.status(500).json({ ok:false, mensaje: 'No se ha podido actualizar el usuario', errors: error })
    }
}

//===================================================
// Borrar un usuario
//===================================================
exports.deleteUser = async (req, res) => {
    let idUsuario = req.params.id

    try {
        let usuarioDB = await Usuario.findByIdAndDelete(idUsuario)

        usuarioDB.password = ':)'
        res.status(200).json({ ok: true, mensaje: 'Usuario Borrado', usuarioDB})
    } catch (error) {
        res.status(404).json({ ok:false, mensaje: 'Usuario no existe', errors: error})
    }
}

