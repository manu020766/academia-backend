const Materia = require('../models/materia')


//===================================================
// Obtener todos los usuarios
//===================================================
exports.getAllSubjects = async (req, res)=> {
    try {
        const materias = await Materia.find().sort('nombre')
        res.status(200).json({ ok: true, materias})
    } catch (error) {
        res.status(500).json({ ok: false, mensaje: 'Error recuperando materias', error })
    }
}

//===================================================
// Obtener una materia
//===================================================
exports.getSubjectById = async (req, res)=> {
    try {
        const materia = await Materia.findById(req.params.id) //.select('categoria subCategoria nombre -_id')
        res.status(200).json({ ok: true, materia })
    } catch (error) {
        res.status(400).json({ ok: false, mensaje: 'Error recuperando materia', error })
    }       
}

//===================================================
// Crear una materia
//===================================================
exports.createSubject = async (req, res)=> {
    try {
        const materia = await Materia.create(req.body)
        res.status(201).json({ ok: true, materia })
    } catch (error) {
        res.status(400).json({ok: false, mensaje: 'Error guardando materia', errors: error})
    }
}

//===================================================
// Actualizar una materia
//===================================================
exports.updateSubject = async (req, res)=> {
    const idMateria = req.params.id
    const modificar = { ...req.body }

    try {
        const materia = await Materia.findByIdAndUpdate(idMateria, modificar)

        res.status(200).json({ ok:true, mensaje: 'materia actualizada', modificar, materia })
    } catch (error) {
        res.status(500).json({ ok:false, idMateria, modificar , mensaje: 'No se ha podido actualizar la materia', error })
    }
}

//===================================================
// Borrar una materia
//===================================================
exports.deleteSubject = async (req, res) => {
    try {
        const materiaDB = await Materia.findByIdAndDelete(req.params.id)
        res.status(200).json({ ok: true, mensaje: 'materia Borrada', materiaDB})
    } catch (error) {
        res.status(404).json({ ok:false, mensaje: 'materia no existe', errors: error})
    }
}

