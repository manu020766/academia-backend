const Profesor = require('../models/profesor')

//================================
// Obtener todos los profesores
//================================
exports.getAllTeachers = async (req, res) => {
    try {
        const profesores = await Profesor.find()
        res.status(200).json({ ok:true, profesores })
    } catch (error) {
        res.status(404).json({ ok:false, mensaje: 'No se han podido recuperar los profesores' })
    }
}

//================================
// Obtener profesor por Id
//================================
exports.getTeacherById = async (req, res) => {
    try {
        const idProfesor = req.params.id

        const profesor = await Profesor.findById(idProfesor)
        res.status(200).json({ ok:true, profesor })
    } catch (error) {
        res.status(404).json({ ok:false, mensaje: 'No se han podido recuperar el profesor' })
    }
}

//================================
// Crea profesor
//================================
exports.createTeacher = async (req, res) => {
    try {
        const nuevoProfesor =  new Profesor({ ...req.body })  

        const profesor = await nuevoProfesor.save()
        res.status(201).json({ ok:true, profesor })
    } catch (error) {
        res.status(500).json({ ok:false, mensaje: 'No se han podido crear el profesor', error })
    }
}

//================================
// Modifica profesor
//================================
exports.updateTeacher = async (req, res) => {
    try {
        const idProfesor = req.params.id
        const actualizar = { ...req.body }

        const profesor = await Profesor.findByIdAndUpdate(idProfesor, actualizar)
        res.status(200).json({ ok:true, actualizar, profesor })
    } catch (error) {
        res.status(404).json({ ok:false, mensaje: 'No se han podido crear el profesor', error })
    }
}

//================================
// Borra profesor
//================================
exports.deleteTeacher = async (req, res) => {
    try {
        const idProfesor = req.params.id

        const profesor = await Profesor.findByIdAndRemove(idProfesor)
        res.status(200).json({ ok:true, profesor })
    } catch (error) {
        res.status(404).json({ ok:false, mensaje: 'No se han podido borrar el profesor' })
    }
}