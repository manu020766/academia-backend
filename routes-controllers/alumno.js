const Alumno = require('../models/alumno')

//===================================================
// Obtener todos los alumnos
//===================================================
exports.getAllPupils = async (req, res)=> {
    try {
        let alumnos

        if (!req.query.activo) {
            alumnos = await Alumno.find()
        } else {
            const parametrosBusqueda = { activo: req.query.activo }
            alumnos = await Alumno.find(parametrosBusqueda)
        }
        res.status(200).json({ ok: true, alumnos})
    } catch (error) {
        res.status(500).json({ ok: false, mensaje: 'Error recuperando alumnos', error })
    }
}

//===================================================
// Obtener un alumno
//===================================================
exports.getPupilById = async (req, res) => {
    try {
        const idPupil = req.params.id

        const alumno = await Alumno.findById(idPupil)
        res.status(200).json({ ok: true, alumno })
    } catch (error) {
        res.status(404).json({ ok:false, mensaje: 'No se pudo encontrar alumno', error })
    }
}

//===================================================
// Crear Alumno
//===================================================

exports.createPupil = async (req, res) => {
    try {
        const nuevoAlumno = new Alumno({ ...req.body })

        const alumno = await nuevoAlumno.save()
        res.status(201).json({ ok: true, alumno })      
    } catch (error) {
        res.status(500).json({ ok:false, mensaje: 'No se ha podido crear alumno', error })
    }
}

//===================================================
// // Actualizar un Alumno
//===================================================
exports.updatePupil = async (req, res) => {
    try {
        const idPupil = req.params.id
        const modificar = { ...req.body }

        const alumno = await Alumno.findByIdAndUpdate(idPupil, modificar)
        res.status(200).json({ ok:true, modificar, alumno })
    } catch (error) {
        res.status(500).json({ ok:false, mensaje: 'No se ha podido actualizar el alumno' })
    }
}

//===================================================
// Borrar un alumno
//===================================================
exports.deletePupil = async (req, res) => {
    try {
        const idPupil = req.params.id

        const alumno = await Alumno.findByIdAndDelete(idPupil)
        res.status(200).json({ ok: true, alumno })
    } catch (error) {
        res.status(500).json({ ok:false, mensaje: 'No se ha podido borrar el alumno' })
    }
}
