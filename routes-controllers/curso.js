const Curso = require('../models/curso')

//==========================================
// Obtener todos los cursos
//==========================================
exports.getAllCourses = async (req, res) => {
    try {
        const cursos = await Curso.find()
            .populate('profesores', 'nombre apellidos')
            .populate('alumnos', 'nombre apellidos')
            .populate('horas_impartidas.profesor', 'nombre apellidos')
        res.status(200).json({ ok: true, cursos })
    } catch (error) {
        res.status(500).json({ ok:false, mensaje: 'No se pueden recuperar cursos', error })
    }
}

//==========================================
// Obtener un curso
//==========================================
exports.getCourseById = async (req, res) => {
    try {
        const idCurso = req.params.id

        const curso = await Curso.findById(idCurso)
            .populate('profesores', 'nombre apellidos')
            .populate('alumnos', 'nombre apellidos')
            .populate('horas_impartidas.profesor', 'nombre apellidos')
        res.status(200).json({ ok: true, curso })
    } catch (error) {
        res.status(500).json({ ok:false, mensaje: 'No se puede recuperar el curso', error })
    }
}

//==========================================
// Crear un curso
//==========================================
exports.createCourse = async (req, res) => {

    try {
        const nuevoCurso = new Curso({ ...req.body })

        const curso = await nuevoCurso.save()
        res.status(201).json({ ok:true, curso })
    } catch (error) {
        res.status(400).json({ ok:false, mensaje: 'No se puede grabar el curso', error })
    }
}

//==========================================
// Actualizar un curso
//==========================================
exports.updateCourse = async (req, res) => {
    try {
        const idCurso = req.params.id
        const actualizar = { ...req.body }

        const curso = await Curso.findByIdAndUpdate(idCurso, actualizar)
        res.status(200).json({ ok:true, actualizar, curso })
    } catch (error) {
        res.status(400).json({ ok:false, mensaje: 'No se puede actualizar el curso', error })
    }
}

//==========================================
// Borrar un curso
//==========================================
exports.deleteCourse = async (req, res) => {
    try {
        const idCurso = req.params.id

        const curso = await Curso.findByIdAndRemove(idCurso)
        res.status(200).json({ ok:true, curso })        
    } catch (error) {
        res.status(400).json({ ok:false, mensaje: 'No se puede borrar el curso', error })
    }

}